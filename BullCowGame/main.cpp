#include <iostream>
#include <string>
#include "FBullCowGame.h"

int main();

void PrintIntro();
void PlayGame();
void PrintGuess(std::string &Guess);
std::string GetGuess();
bool AskToPlayAgain();


FBullCowGame BCGame;

int main()
{
	do
	{
		PrintIntro();
		PlayGame();
	} while (AskToPlayAgain());

	return 0;
}

void PrintIntro()
{
	constexpr int WORD_LENGTH = 5;

	std::cout << "Welcome to Bulls and Cows!\n";
	std::cout << "Are you able to guess the " << WORD_LENGTH;
	std::cout << " letter isogram?\n";
	std::cout << std::endl;

	return;
}

void PlayGame()
{
	BCGame.Reset();
	int MaxTries = BCGame.GetMaxTries();

	// TODO change to WHILE loop to keep looping if guess is invalid
	for (int i = 0; i < MaxTries; i++)
	{
		std::string Guess = GetGuess(); 
		bool isValid = BCGame.IsValidGuess(Guess);

		if(isValid)
			// check guess score (bulls & cows)
			// print score

		PrintGuess(Guess);
	}

	// TODO Add game summary
}

std::string GetGuess()
{
	std::cout << "Try " << BCGame.GetCurrentTry() << ". Enter guess: ";
	std::string Guess = "";
	std::getline(std::cin, Guess);

	return Guess;
}


bool AskToPlayAgain()
{
	std::cout << "Do you want to play again? [Y/n]" << std::endl;

	std::string Response = "";
	std::getline(std::cin, Response);

	return (Response[0] == 'y' || Response[0] == 'Y');
}

void PrintGuess(std::string &Guess)
{
	std::cout << "Your guess was: ";
	std::cout << Guess << std::endl;
	std::cout << std::endl;
}
