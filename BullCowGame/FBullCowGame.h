#pragma once

#include <string>

class FBullCowGame
{
public:
	FBullCowGame();

	int GetMaxTries() const;
	int GetCurrentTry() const;
	bool IsGameWon() const;
	bool IsValidGuess(std::string) const;
	void Reset();
	// add method for counting bulls and cows and incrementing try #

private:
	int MyCurrentTry;
	int MyMaxTries;

};